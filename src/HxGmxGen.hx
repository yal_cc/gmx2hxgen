package ;

/**
 * ...
 * @author YellowAfterlife
 */
class HxGmxGen {
	static function rec(b:StringBuf, q:SfGmx, z:Int) {
		var qp = z > 0 ? 'q${z-1}' : 'root';
		var qz = 'q$z';
		var qz1 = 'q${z+1}';
		var keys = q.keys();
		var name = q.name;
		var sep = "\n" + StringTools.rpad("", "\t", z);
		if (q.children.length > 0) {
			if (z > 0) {
				b.add('${sep}$qz = $qp.addEmptyChild("$name");');
			} else b.add('var $qz = new SfGmx("$name");');
			b.add('${sep}for (_ in []) {');
			b.add('${sep}\tvar $qz1:SfGmx;');
			var found = new Map();
			for (c in q.children) {
				if (found.exists(c.name)) continue;
				found[c.name] = true;
				rec(b, c, z + 1);
			}
			b.add('${sep}}');
		} else {
			b.add(sep);
			var fn:String;
			if (keys.length > 0) b.add('$qz = ');
			if (q.text != null) {
				var f = Std.parseFloat(q.text);
				b.add('$qp.');
				if (!Math.isNaN(f)) {
					if (Std.is(f, Int)) {
						b.add('addIntChild("$name", 0);');
					} else {
						b.add('addFloatChild("$name", 0);');
					}
				} else {
					b.add('addTextChild("$name", null);');
				}
			} else b.add('addEmptyChild("$name");');
		}
		for (key in keys) {
			b.add('${sep}$qz.set("$key", "");');
		}
	}
	public static function print(root:SfGmx) {
		var b = new StringBuf();
		rec(b, root, 0);
		return b.toString();
	}
}