package;

import haxe.Http;
import js.Browser;
import js.Lib;
import js.html.Element;
import js.html.InputElement;
import js.html.TextAreaElement;

/**
 * ...
 * @author YellowAfterlife
 */
class Main {
	static inline function find<T:Element>(id:String):T {
		return cast Browser.document.getElementById(id);
	}
	static function main() {
		var input:TextAreaElement = find("input");
		var output:TextAreaElement = find("output");
		function proc() {
			try {
				var v = input.value;
				var gmx = SfGmx.parse(v);
				var r = "";
				r = HxGmxGen.print(gmx);
				output.value = r;
			} catch (x:Dynamic) {
				output.value = x;
			}
		}
		input.onchange = function(_) proc();
		#if (debug && js)
		var req = new Http("test.gmx");
		req.onData = function(s:String) {
			input.value = s;
			proc();
		}
		req.request();
		#end
	}
	
}