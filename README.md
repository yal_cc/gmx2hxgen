# gmx2hxgen

This tool takes GMX (GameMaker: Studio XML) snippets and generates Haxe boilerplate code that would generate such snippets.

If your first thought is "why would I even want that", you should admire your life going well so far.

(this allows to reduce the volume of manually written code when generating GM:S related resources, and to avoid human error in doing so)

## Limitations

The output is pretty janky and intended for editing - GM:S is very inconsistent with how it treats different GMX elements, so it's mostly impossible to tell if something is a list of child nodes or duplicates of the same property-node, for example.

## Credits & License

Tool by [YellowAfterlife](https://yal.cc).

Contains code from my own sfhx/sfgml libraries.

Licensed under MIT.
